<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Login</title>
 <link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<!-- Optional theme -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css">
<!-- Latest compiled and minified JavaScript -->
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<script src="//code.jquery.com/jquery.min.js"></script>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
<body>
<div class="ex">
		
		<form action="loginProcess" method="post">
			<table cellpadding="5">
				<tr>
					<td>ID</td>
					<td><input type="text" class="form-control" name="id" placeholder="ID" required/> </td>
				</tr>
				<tr>
					<td>PASSWORD</td>
					<td><input type="password" class="form-control" name="password" placeholder="PASSWORD"/> </td>
				</tr>
				
				<tr>
					<td></td>
					<td><input type="submit" class="btn btn-primary" value="login"/> </td>
				</tr>
				
					
			</table>
				
		</form>
		
		<div class="container" style="margin-bottom: 16px; padding: 20px;">
			<div class="container-fluid">
				<div class="pull-right">
					<a href="/boardProject/join">Join</a>
				</div>
			</div>
		</div>
		
	</div>
</body>
</html>