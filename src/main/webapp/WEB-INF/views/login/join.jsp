<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script src="//code.jquery.com/jquery.min.js"></script>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
<link
	href="http://netdna.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.css"
	rel="stylesheet">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css">
<script
	src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.js"></script>
<script
	src="http://netdna.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.js"></script>
<title>Join</title>
</head>
<body>
	<script type="text/javascript" src="resources/js/userJoin.js"></script>
	<div class="container">
		<c:set var="users" value="${users}" />
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				<form name="joinForm" method="POST" action="/boardProject/join/joinDo">

					<fieldset>
						<legend>Account Details</legend>
						<div class="form-group col-md-6">
							<label for="id">ID</label>
							<input type="text" style="IME-MODE: disabled" class="form-control" name="user_id" id="userId"/>
							
						</div>
						<div class="col-md-6">
							<button type="button" onclick="duplicationCheck();"class="btn btn-primary">check</button>
						</div>
						<div class="form-group col-md-8">
							<label for="first_name">Name</label>
							<input type="text" class="form-control" name="user_name" id="name"/>
						</div>

						<div class="form-group col-md-6">
							<label for="password">Password</label>
							<input type="password" style="IME-MODE: disabled" class="form-control" name="user_password" id="password"/>
						</div>

					</fieldset>

					<div class="form-group">
						<div class="col-md-12">
							<button type="submit" class="btn btn-primary">Register</button>
						</div>
					</div>
					
					<input type="hidden" id="users" name="users" value='${users}'>
					
				</form>
			</div>

		</div>
	</div>
</body>
</html>