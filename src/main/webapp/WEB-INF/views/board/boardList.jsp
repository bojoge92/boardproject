<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link
	href="http://netdna.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.css"
	rel="stylesheet">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css">
<script
	src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.js"></script>
<script
	src="http://netdna.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.js"></script>

<title>Board List</title>
</head>
<body>
	<script type="text/javascript" src="resources/js/boardList.js"></script>
	<c:set var="boardList" value="${boardList}" />
	<c:set var="tags" value="${tag}" />

	<div class="container" style="margin-bottom: 16px; padding: 20px;">
		<div class="container-fluid"></div>
	</div>

	<div class="container">
		<div class="container" style="margin-bottom: 16px; padding: 20px;">
			<div class="container-fluid">
				<div class="pull-right">
					<a href="/boardProject/logout">logout</a>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="table-responsive">
				<table class="table table-hover">
					<thead>
						<tr>
							<th>#</th>
							<th>title</th>
							<th>writer</th>
							<th>date</th>
						</tr>
					</thead>
					<tbody id="myTable">
						<c:forEach items="${boardList}" var="bl" varStatus="status">
							
							<tr class='clickable-row'
								onclick="location.href='/boardProject/boardPost/${bl.board_num}';">
								
								<td><p title="${tags[status.index]}"><c:out value="${fn:length(boardList)-status.index}" /></p></td>
								<td><p title="${tags[status.index]}"><c:out value="${bl.board_title}" /></p></td>
								<td><p title="${tags[status.index]}"><c:out value="${bl.user_name}" /></p></td>
								<td><p title="${tags[status.index]}"><c:out value="${bl.board_date}" /></p></td>
							</tr>
			
							
						</c:forEach>
					</tbody>
				</table>
			</div>

			<div class="pull-right">
				<input type="button"
					onclick="location.href='/boardProject/boardWriter'"
					class="btn btn-primary" value="New" />
			</div>

			<div class="col-md-12 text-center">
				<ul class="pagination pagination-lg pager" id="myPager"></ul>
			</div>

		</div>
	</div>


</body>
</html>