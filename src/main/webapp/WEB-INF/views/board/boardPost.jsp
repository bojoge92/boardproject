<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>board Post</title>

<link
	href="http://netdna.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.css"
	rel="stylesheet">
<script
	src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.js"></script>
<script
	src="http://netdna.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.js"></script>
<link
	href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.2/summernote.css"
	rel="stylesheet">
<script
	src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.2/summernote.js"></script>

</head>

<body>
	<script type="text/javascript" src="../resources/js/boardPost.js"></script>

	<c:set var="board" value="${boardContent}" />
	<div class="container">
		<div class="row">
			<div style="margin: 5%">
				<div class="col-sm-8 text-center pull-right">
					<button id="delete"
						onclick="page_move();"
						class="btn btn-primary">delete</button>
					<button id="edit" class="btn btn-primary"
						onclick="editPost();">edit</button>
				</div>
				<div class="col-xs-6 col-sm-8">
					<div class="page-header col-sm-12">
						<h2>
							<c:out value="${board.board_title}" />
						</h2>
						<div class="info">
							<p>
								<c:out value="${board.board_date}" />
								by
							<p>
						</div>
						<p>
							<c:out value="${board.user_name}" />
						</p>
					</div>
					<form name="ff" method="POST">
						<input type="hidden" id="board_num" name="board_num" value='${board.board_num}'>
						<input type="hidden" id="user_id" name="user_id" value='${board.user_id}'>
						<input type="hidden" id="board_file_name" name="board_file_name" value='${board.board_file_name}'>
					</form>
					<br>
					<br>
					<div class="col-xs-6 col-sm-12">
						<p>
							<a href="#" onclick="downloadFile();">${board.board_file_name}</a>
						</p>
					</div>

					<div class="col-xs-6 col-sm-12">
						<div class="click2edit">${board.board_content}</div>
					</div>

					<div class="col-xs-6 col-sm-12">
						<div class="saveButton" style="display: none">
							<button id="save" class="btn btn-primary" onclick="save()">Save</button>
						</div>
					</div>

				</div>
		<div class="container" style="margin-bottom: 16px; padding: 20px;">
			<div class="container-fluid">
				<div class="pull-right">
					<a href="/boardProject/boardList">go to List</a>
				</div>
			</div>
		</div>
			</div>
		</div>
	</div>

</body>
</html>
