<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Summernote</title>
<link
	href="http://netdna.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.css"
	rel="stylesheet">
<script
	src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.js"></script>
<script
	src="http://netdna.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.js"></script>
<link
	href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.2/summernote.css"
	rel="stylesheet">
<script
	src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.2/summernote.js"></script>
</head>
<body>
	<script type="text/javascript" src="resources/js/boardWriter.js"></script>
	<script type="text/javascript" src="resources/js/html2canvas.js"></script>
	
	<div id="target2"></div>
	
	<div style="margin: 5%;" id ="chart_div">
		<form action="/boardProject/write" method="POST" enctype="multipart/form-data" id="fom">
			<div class="form-group" style="margin-bottom: 3%;">
				<label for="usr">Title:</label>
				<input type="text" class="form-control" id="title" name="board_title">
			</div>


			<div class="click2edit">
				<textarea id="boardContent" name="board_content"></textarea>
			</div>

			<input type="file" name="file" size="50"> 
			<br />
			<br />
			<div class="col-sm-12 text-center">
				<button id="submit" class="btn btn-primary" type="submit" >Submit</button>
				<button onclick="cancelCheck()" class="btn btn-primary">Cancel</button>
			</div>
		</form>
	</div>
	
	
	<script>
		$('#boardContent').summernote({
			lang : 'ko-KR',
			height : 300,
			minHeight : null,
			maxHeight : null,
			focus : true
		});
	</script>
</body>
</html>