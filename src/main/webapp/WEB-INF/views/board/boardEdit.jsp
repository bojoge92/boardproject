<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Edit Page</title>
<link
	href="http://netdna.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.css"
	rel="stylesheet">
<script
	src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.js"></script>
<script
	src="http://netdna.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.js"></script>
<link
	href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.2/summernote.css"
	rel="stylesheet">
<script
	src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.2/summernote.js"></script>
</head>
<body>
	<script type="text/javascript" src="../resources/js/boardWriter.js"></script>
	<c:set var="board" value="${boardContent}" />
	<div style="margin: 5%;">
		<form action="/boardProject/update" method="POST">
			<div class="form-group" style="margin-bottom: 3%;">
				<label for="usr">Title:</label>
				<input type="text" class="form-control" id="title" name="board_title" value="${board.board_title}">
				<input type="hidden" name="board_date" value='${board.board_date}' >
				<input type="hidden" name="board_num" value='${board.board_num}' >
				<input type="hidden" name="user_id" value='${board.user_id}' >
				<input type="hidden" name="user_name" value='${board.user_name}' >
				<input type="hidden" name="user_name" value='${board.board_file_name}' >
			</div>


			<div class="click2edit">
				<textarea id="boardContent" name="board_content">${board.board_content}</textarea>
			</div>

			<div class="col-sm-12 text-center">
				<button id="edit" class="btn btn-primary">save</button>
				<button onclick="cancelCheck()" class="btn btn-primary">Cancel</button>
			</div>
		</form>
	</div>
	<script>
		$('#boardContent').summernote({
			lang: 'ko-KR',
			height : 300,
			minHeight : null, 
			maxHeight : null, 
			focus : true
		});
	</script>
</body>
</html>