package com.navercorp.boardProject.file.service;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.commons.io.IOUtils;
import org.apache.tomcat.util.codec.binary.Base64;

public class FileUtil {
	
		
	/**
	 * 첨부된 파일을 서버에 저장한다.
	 * 
	 * @param directory경로
	 * @param fileName
	 * @param bytes(바이너리 파일)
	 * @return file path
	 * @throws IOException
	 */
	public static String saveFile(String dirStr, String fileName, byte[] bytes) throws IOException {
		File dir = new File(dirStr);
		if (!dir.exists()) {
			dir.mkdirs();
		}

		File serverFile = new File(dir.getAbsolutePath() + File.separator + fileName);
		
		BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(serverFile));
		IOUtils.write(bytes, stream);
		IOUtils.closeQuietly(stream);
		
		return dir.getAbsolutePath() + File.separator + fileName;
	}
	
	/**
	 * 파일 패스를 통해 이미지를 바이너리로 변환한다.
	 * 
	 * @param fileDir
	 * @param filePath
	 * @return 바이너리 String
	 * @throws IOException
	 */
	public static String fileToBinaryImage(String fileDir, String filePath) throws IOException {
		String filePathName = fileDir + filePath;
		String fileExtName = filePathName.substring(filePathName.lastIndexOf(".") + 1);
		String imageString = "";
		FileInputStream inputStream = null;
		ByteArrayOutputStream byteOutStream = null;
		String binaryString ="";

		try {
			File file = new File(filePathName);

			if (file.exists()) {
				inputStream = new FileInputStream(file);
				byteOutStream = new ByteArrayOutputStream();

				int len = 0;
				byte[] buf = new byte[1024];
				while ((len = inputStream.read(buf)) != -1) {
					byteOutStream.write(buf, 0, len);
				}

				byte[] fileArray = byteOutStream.toByteArray();
				
				imageString = new String(Base64.encodeBase64(fileArray));

				binaryString = "data:image/" + fileExtName + ";base64, " + imageString;
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			inputStream.close();
			byteOutStream.close();
		}
		return binaryString;
	}
	
}
