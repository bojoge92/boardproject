package com.navercorp.boardProject.security;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class SHACryptography {
	
	
	
	/**
	 * 비밀번호 관련 암호화 진행
	 * @param password
	 * @return sha된 password
	 */
	public static String doCrypto(String password) {
		String SHA = "";
		try {
			MessageDigest sh = MessageDigest.getInstance("SHA-256");
			sh.update(password.getBytes());
			byte byteData[] = sh.digest();
			StringBuffer sb = new StringBuffer();
			for (int i = 0; i < byteData.length; i++) {
				sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
			}
			SHA = sb.toString();

		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
			SHA = null;
		}
		return SHA;
	}
}
