package com.navercorp.boardProject.board.repository;


import java.util.ArrayList;

import org.springframework.stereotype.Repository;

import com.navercorp.boardProject.board.model.Board;


@Repository
public interface BoardRepository {
	
	/**
	 * 게시물 board객체를 저장한다.
	 * 
	 * @param board
	 */
	public void insertBoard(Board board);
	
	/**
	 * board의 리스트를 가져온다.
	 * 
	 * @return board
	 */
	public ArrayList<Board> selectBoardList();

	/**
	 * 해당하는 게시물의 내용을 가져온다.
	 * 
	 * @param boardNum
	 * @return boardContent
	 */
	public Board selectBoardContent(int boardNum);

	/**
	 * 해당 개시물을 삭제한다.
	 * 
	 * @param boardNum
	 */
	public void deleteBoardPost(int boardNum);
	
	/**
	 * 게시물을 update한다.
	 * 
	 * @param board
	 */
	public void updateBoardPost(Board board);
}
