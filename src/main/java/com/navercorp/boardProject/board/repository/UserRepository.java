package com.navercorp.boardProject.board.repository;

import java.util.ArrayList;

import org.springframework.stereotype.Repository;
import com.navercorp.boardProject.board.model.User;

@Repository
public interface UserRepository {
	
	/**
	 * User객체를 DB에 저장한다.
	 * 
	 * @param user
	 */
	public void insertUser(User user);
	
	/**
	 * id에 해당하는 User객체를 가져온다.
	 * 
	 * @param userId
	 * @return User
	 */
	public User selectUser(String userId);

	/**
	 * id에 해당하는 userName을 반환한다.
	 * 
	 * @param userId
	 * @return userName
	 */
	public String selectUserName(String userId);
	
	/**
	 * user ID를 가져온다.
	 * 
	 * @return userid
	 */
	public ArrayList<String> selectUserIdList();
	
}
