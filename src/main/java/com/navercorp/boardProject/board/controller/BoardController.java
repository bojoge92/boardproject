package com.navercorp.boardProject.board.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.navercorp.boardProject.board.model.Board;
import com.navercorp.boardProject.board.model.User;
import com.navercorp.boardProject.board.service.BoardService;

@Controller
public class BoardController {

	@Autowired
	private BoardService boardService;
	
	@Value("#{file['file.dir']}")
	private String fileDir;
	
	@RequestMapping(value = "/boardProject")
	public String goHome(Model model) {
		return "/board/boardList";
	}

	@RequestMapping(value = "/boardWriter")
	public String goBoardEditor(Model model) {
		return "/board/boardWriter";
	}

	/**
	 * 게시판의 목록을 보여
	 * 
	 * @param board
	 * @param model
	 * @return
	 */
	@Secured("ROLE_USER")
	@RequestMapping(value = "/boardList")
	public String goBoardList(@ModelAttribute Board board, Model model) {
		ArrayList<Board> boardList = boardService.selectBoardList();
		Collections.reverse(boardList);
		model.addAttribute("boardList", boardList);
		model.addAttribute("tag", boardService.boardTag(boardList));
		return "/board/boardList";
	}

	/**
	 * 게시글을 보여준다.
	 * 
	 * @param boardNum
	 * @param model
	 * @return
	 * @throws IOException
	 */
	@RequestMapping(value = "/boardPost/{board_id}", method = RequestMethod.GET)
	public String goBoardShow(@PathVariable(value = "board_id") int boardNum, Model model) throws IOException {
		model.addAttribute("boardContent", boardService.selectBoardContent(boardNum));
		return "/board/boardPost";
	}

	/**
	 * 게시물을 수정한다.
	 * 
	 * @param boardNum
	 * @param model
	 * @return
	 * @throws IOException
	 */
	@RequestMapping(value = "/boardEdit", method = RequestMethod.POST)
	public String goBoardEdit(@RequestParam(value = "board_num") int boardNum, @RequestParam(value = "user_id") String boardUserID, Model model, HttpSession session) throws IOException {
		User user = (User) session.getAttribute("userLoginInfo");
		model.addAttribute("boardContent", boardService.selectBoardContent(boardNum));
		return boardService.editBoardPost(boardNum, boardUserID, user);
	}

	/**
	 * 게시글을 등록한다.
	 * 
	 * @param board
	 * @param file
	 * @param session
	 * @return
	 * @throws IOException
	 */
	@RequestMapping(value = "/write", method = RequestMethod.POST)
	public String goWrite(@ModelAttribute Board board, @RequestParam("file") MultipartFile file, HttpSession session) throws IOException {
		User user = (User) session.getAttribute("userLoginInfo");
		boardService.saveBoardPost(board, file, user);
		return "redirect:/boardList";
	}

	/**
	 * 게시글을 삭제한다.
	 * 
	 * @param boardNum
	 * @param boardUserID
	 * @param session
	 * @return
	 */
	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	public String goDelete(@RequestParam(value = "board_num") int boardNum, @RequestParam(value = "user_id") String boardUserID, HttpSession session) {
		User user = (User) session.getAttribute("userLoginInfo");
		return boardService.deleteBoardPost(boardNum, boardUserID, user);
	}

	/**
	 * 게시물 수정후 저장한다.
	 * 
	 * @param board
	 * @return
	 */
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public String goUpdate(@ModelAttribute Board board) {
		boardService.updateBoardPost(board);
		return "redirect:/boardList";
	}
	
	/**
	 * 게시물의 첨부파일을 다운로드한다.
	 * 
	 * @param filePath
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(value="/getFile", method = RequestMethod.POST)
	public void getFile(@RequestParam(value = "board_file_name") String filePath, HttpServletResponse response) throws IOException {
		boardService.downloadFile(filePath, response);
	}
}
