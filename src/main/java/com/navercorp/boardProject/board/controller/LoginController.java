package com.navercorp.boardProject.board.controller;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.navercorp.boardProject.board.model.User;
import com.navercorp.boardProject.board.service.UserService;
import com.navercorp.boardProject.security.SHACryptography;

@Controller
public class LoginController {

	@Autowired
	UserService userService;
	
	/**
	 * 회원가입창을 불러온다.
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/join")
	public String goJoin(Model model) {
		model.addAttribute("users", userService.selectUserIdList());
		return "/login/join";
	}

	/**
	 * 로그인 페이지를 불러온다.
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/loginPage")
	public String goLogin(Model model) {
		return "/login/login";
	}

	
	/**
	 * 회원가입자 정보를 저장한다.
	 * 
	 * @param user
	 * @return
	 */
	@RequestMapping(value = "/join/joinDo")
	public String goJoinDo(@ModelAttribute User user) {
		userService.insertUser(user);
		return "redirect:/";
	}


	/**
	 * 로그인 성공후 User의 userName을 갖고 session에 저장한다.
	 * 
	 * @param session
	 * @return
	 */
	@RequestMapping(value = "/login_success", method = RequestMethod.GET)
	public String goLoginSuccess(HttpSession session) {
		User user = (User) SecurityContextHolder.getContext().getAuthentication().getDetails();
		user.setUser_name(userService.selectUserName(user.getUser_id()));
		user.setUser_password(SHACryptography.doCrypto(user.getPassword()));
		session.setAttribute("userLoginInfo", user);
		return "redirect:/boardList";
	}

	/**
	 * 로그아웃 시 세션을 삭제한다.
	 * 
	 * @param session
	 */
	@RequestMapping(value = "/logout", method = RequestMethod.GET)
	public void logout(HttpSession session) {
		session.invalidate();
	}

	@RequestMapping(value = "/login_duplicate", method = RequestMethod.GET)
	public void login_duplicate() {
		System.out.println("Welcome login_duplicate!");
		
	}

}
