package com.navercorp.boardProject.board.service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.navercorp.boardProject.board.model.User;
import com.navercorp.boardProject.board.repository.UserRepository;
import com.navercorp.boardProject.security.SHACryptography;

@Service
public class UserService {
	@Autowired
	private UserRepository userRepository;
	

	/**
	 * User객체를 DB에 저장
	 * 
	 * @param user
	 */
	public void insertUser(User user) {
		user.setUser_password(SHACryptography.doCrypto(user.getUser_password()));
		userRepository.insertUser(user);
	}

	/**
	 * 아이디 중복체크
	 * 
	 * @param userId
	 * @return 중복여부
	 */
	public boolean checkUserId(String userId) {
		User user = userRepository.selectUser(userId);
		if (user == null) {
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * id에 맞는 유저에 대한 정보를 가져온다.
	 * 
	 * @param userId
	 * @return user
	 */
	public User selectUser(String userId) {
		return userRepository.selectUser(userId);
	}
	
	/**
	 * User의 이름 반환한다.
	 * 
	 * @param userId
	 * @return userName
	 */
	public String selectUserName(String userId) {
		return userRepository.selectUserName(userId);
	}
	
	/**
	 * 로그인을 수행한다.
	 * 
	 * @param id
	 * @param pwd
	 * @return 성공여부
	 */
	public boolean login(String id, String pwd) {
		User user = selectUser(id);
		if(user.getUser_id().equals(id) && user.getUser_password().equals(SHACryptography.doCrypto(pwd))){
			return true;
		} else
			return false;
	}
	
	/**
	 * 전체 User의 id리스트를 가져온다.
	 * 
	 * @return User List
	 */
	public ArrayList<String> selectUserIdList() {
		return userRepository.selectUserIdList();
	}

}
