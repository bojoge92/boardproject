package com.navercorp.boardProject.board.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.DatatypeConverter;

import org.jsoup.Jsoup;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.navercorp.boardProject.board.model.Board;
import com.navercorp.boardProject.board.model.User;
import com.navercorp.boardProject.board.repository.BoardRepository;
import com.navercorp.boardProject.file.service.FileUtil;
import com.navercorp.boardProject.parsing.service.Parser;

@Service
public class BoardService {

	@Autowired
	private BoardRepository boardRepository;
	
	@Value("#{file['file.dir']}")
	private String fileDir;

	/**
	 * board에 파일이 있을경우 파일을 저장하고 board_file_name을 저장한다.
	 * 
	 * @param board 객체
	 * @param file
	 * @param user 객체
	 * @throws IOException
	 */
	public void saveBoardPost(Board board, MultipartFile file, User user) throws IOException {
		if (!file.isEmpty()) {
			FileUtil.saveFile(fileDir + File.separator + "File", file.getOriginalFilename(), file.getBytes());
			board.setBoard_file_name(file.getOriginalFilename());
		}
			insertPost(board, user);
	}

	/**
	 * 게시글을 작성하고 editor로 부터 img태그에서 src의 바이너리를 뽑아 저장
	 * 이미지가 주소로써 첨부되었을 경우 그대로 반환한다.
	 * 
	 * @param board 객체
	 * @return 저장한 image path
	 * @throws IOException
	 */
	private String saveImageSRCServer(Board board, User user) throws IOException {
		String picSRC = Jsoup.parse(board.getBoard_content()).select("img").attr("src");
		if (!picSRC.contains("http://")) {
			String picBinary = picSRC.split(",")[1];
			String picName = Jsoup.parse(board.getBoard_content()).select("img").attr("data-filename");
			
			String fileName = user.getUser_id() + "_" + picName;

			FileUtil.saveFile(fileDir + File.separator + "Img", fileName, DatatypeConverter.parseBase64Binary(picBinary));

			return File.separator + "Img" + File.separator + fileName;
		}
		return picSRC;
	}

	/**
	 * 이미지 태그 변환 후 입력된 board객체를 저장한다.
	 * 
	 * @param board 객체
	 * @param user 객체
	 * @throws IOException
	 */
	public void insertPost(Board board, User user) throws IOException {
		board.setUser_id(user.getUser_id());
		board.setUser_name(user.getUser_name());

		if (!Jsoup.parse(board.getBoard_content()).select("img").isEmpty()) {
			String imgPath = saveImageSRCServer(board, user);
			board.setBoard_content(board.getBoard_content().replace(Jsoup.parse(board.getBoard_content()).select("img").attr("src"), imgPath));
		}

		boardRepository.insertBoard(board);

	}

	/**
	 * 포스트된 파일에 다운로드를 진행한다.
	 * 
	 * @param filePath
	 * @param response
	 * @throws IOException
	 */
	public void downloadFile(String filePath, HttpServletResponse response) throws IOException {
		File file = new File(fileDir + File.separator + "File" + File.separator + filePath);
		response.setContentType("application/octet-stream");

		String downName = filePath;

		response.setHeader("Content-Disposition", "attachment;filename=\"" + downName + "\";");

		FileInputStream fileInputStream = new FileInputStream(file);
		ServletOutputStream servletOutputStream = response.getOutputStream();

		byte b[] = new byte[1024];
		int data = 0;

		while ((data = (fileInputStream.read(b, 0, b.length))) != -1) {
			servletOutputStream.write(b, 0, data);
		}

		servletOutputStream.flush();
		servletOutputStream.close();
		fileInputStream.close();
	}

	/**
	 * 게시판 목록을 보여주기 위한 board객체를 가져온다.
	 * 
	 * @return board객체 리스트
	 */
	public ArrayList<Board> selectBoardList() {
		return boardRepository.selectBoardList();
	}
	
	/**
	 * 불필요한 태그를 제거한다.
	 * 
	 * @param boardList
	 * @return 게시물의 내용
	 */
	public ArrayList<String> boardTag(ArrayList<Board> boardList) {
		ArrayList<String> result = new ArrayList<String>();
		for(int i=0;i<boardList.size();i++) {
			result.add(i, Parser.removeHtmlTag(boardList.get(i).getBoard_content()));
		}
		return result ;
	}

	/**
	 * 게시물에 대한 내용을 가져온다.
	 * 제목과 내용에 대해 불필요한 태그를 제거한다.
	 * 
	 * 
	 * @param boardNum
	 * @return
	 * @throws IOException
	 */
	public Board selectBoardContent(int boardNum) throws IOException {
		Board board = boardRepository.selectBoardContent(boardNum);
		board.setBoard_title(Parser.htmlTagParser(board.getBoard_title()));
		board.setBoard_content(Parser.htmlTagParser(board.getBoard_content()));
		board.setBoard_content(Parser.htmlImageParser(fileDir, board.getBoard_content()));
		return board;
	}

	public String deleteBoardPost(int boardNum, String boardUserID, User user) {
		if(boardUserID == user.getUser_id()){
			boardRepository.deleteBoardPost(boardNum);
			return "redirect:/boardList";
		} else
			return "redirect:/boardPost/" + boardNum;
	}
	
	public String editBoardPost(int boardNum, String boardUserID, User user) {
		if(boardUserID == user.getUser_id()){
			boardRepository.selectBoardContent(boardNum);
			return "redirect:/boardPost/" + boardNum;
		} else
			return "redirect:/boardEdit/" + boardNum;
	}


	public void updateBoardPost(Board board) {
		boardRepository.updateBoardPost(board);
	}

}
