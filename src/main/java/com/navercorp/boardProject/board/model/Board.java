package com.navercorp.boardProject.board.model;

import java.sql.Timestamp;

public class Board {
	
	private String user_id;
	private String user_name;
	
	private String board_title;
	private String board_content;
	private Timestamp board_date;
	private int board_num;
	private String board_file_name = null;

	public Board() {
		
	}
	
	public String getUser_id() {
		return user_id;
	}

	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}

	public String getBoard_title() {
		return board_title;
	}

	public void setBoard_title(String board_title) {
		this.board_title = board_title;
	}

	
	public String getBoard_content() {
		return board_content;
	}

	public void setBoard_content(String board_content) {
		this.board_content = board_content;
	}

	public Timestamp getBoard_date() {
		return board_date;
	}

	public void setBoard_date(Timestamp board_date) {
		this.board_date = board_date;
	}

	public int getBoard_num() {
		return board_num;
	}

	public void setBoard_num(int board_num) {
		this.board_num = board_num;
	}
	
	public String getUser_name() {
		return user_name;
	}

	public void setUser_name(String user_name) {
		this.user_name = user_name;
	}
	
	public String getBoard_file_name() {
		return board_file_name;
	}

	public void setBoard_file_name(String board_file_name) {
		this.board_file_name = board_file_name;
	}
	
	@Override
	public String toString() {
		return "Board [user_id=" + user_id + ", user_name=" + user_name + ", board_title=" + board_title
				+ ", board_content=" + board_content + ", board_date=" + board_date + ", board_num=" + board_num
				+ ", board_file_name=" + board_file_name + "]";
	}
}
