package com.navercorp.boardProject.parsing.service;

import java.io.IOException;

import org.jsoup.Jsoup;

import com.navercorp.boardProject.file.service.FileUtil;

public class Parser {
	
	/**
	 * String에 불필요한 <script>문을 제거한다.
	 * 
	 * @param str
	 * @return 파싱된 String
	 */
	public static String htmlTagParser(String str) {
		return str.replace("<script>", "&ltscript&gt").replace("<script","&ltscript");
	}
	
	/**
	 * 바이너리 소스를 글 내용에 적용한다.
	 * 
	 * @param fileDir
	 * @param content
	 * @return img태그가 수정된 게시물 내용
	 * @throws IOException
	 */
	public static String htmlImageParser(String fileDir, String content) throws IOException {
		if(!Jsoup.parse(content).select("img").isEmpty()) {
			String imgPath = Jsoup.parse(content).select("img").attr("src");
			if(!imgPath.contains("http://"))
				return content.replace(Jsoup.parse(content).select("img").attr("src"), FileUtil.fileToBinaryImage(fileDir, imgPath));
			else
				return content;
			
		} else {
			return content;
		}
	}
	
	/**
	 * html을 포함한 문자열에 html tag를 제거하고 문자열만 반환한다.
	 * 
	 * @param html
	 * @return 파싱된 문자열
	 */
	public static String removeHtmlTag(String html) {
		return Jsoup.parse(html).text();
	}
	
}
